#! /bin/sh

echo "Configuração do projeto Eventos da Hora"
rm -rf ~/tools
mkdir ~/tools
cp *.zip ~/tools
cd ~/tools
unzip compiled.zip
unzip kafka.zip

cd ~/tools/kafka/bin

sudo chmod +x *.sh

echo "Iniciando zookeeper..."
gnome-terminal -x ./zookeeper-server-start.sh ../config/zookeeper.properties

sleep 5

echo "Iniciando Kafka..."
gnome-terminal -x ./kafka-server-start.sh ../config/server.properties

sleep 5

docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name ticketsdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=ticketsdb -p 5437:5432 postgres:10.5
docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name paymentsdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=paymentsdb -p 5433:5432 postgres:10.5
docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name orderdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=orderdb -p 5435:5432 postgres:10.5
docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name userdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=userdb -p 5401:5432 postgres:10.5
docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name imagedb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=imagedb -p 5402:5432 postgres:10.5

echo "Iniciando micro serviços..."

cd ~/tools/compiled

echo "Iniciando zuul-gateway"
java -jar zuul-gateway/eventos-da-hora-zuul-gateway.jar &
echo "zuul-gateway ok"

echo "Iniciando orquestrador-api"
gnome-terminal -x java -jar orquestrador-api/eventos-da-hora-orquestrador-sagas.jar
echo "orquestrador-api ok"

echo "Iniciando image-api"
java -jar image-api/eventos-da-hora-image-api.jar &
echo "image-api ok"

echo "Iniciando event-api"
gnome-terminal -x java -jar event-api/eventos-da-hora-event-api.jar 
echo "event-api ok"

echo "Iniciando email-api"
java -jar email-api/eventos-da-hora-email-api.jar &
echo "email-api ok"

echo "Iniciando payment-api"
gnome-terminal -x java -jar payment-api/eventos-da-hora-payment-api.jar
echo "payment-api ok"

echo "Iniciando order-api"
gnome-terminal -x java -jar order-api/eventos-da-hora-pedido-api.jar
echo "order-api ok"

echo "Iniciando user-api"
gnome-terminal -x java -jar user-api/eventos-da-hora-user-api.jar
echo "user-api ok"

echo "Iniciando aplicações do front end"

cd ~/tools/compiled/eventos-da-hora-ui
echo "Build do front end do usuário final"
npm install

echo "Subindo front end do usuário final"
node server.js &

echo "Disponível no endereço localhost:4200"

cd ~/tools/compiled/eventos-da-hora-admin-ui

echo "Build do front end do administrador"
npm install

echo "Subindo front end do administrador"
node server.js &

echo "Disponível no endereço localhost:4200"
