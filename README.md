
# Setup da aplicação eventos da hora

Bem vindo ao manual de instruções da aplicação Eventos da Hora.

Aqui você irá encontrar todas as instruções necessárias para subir o projeto na sua máquina local.   
Se atente ao tipo do seu sistema operacional utilizado pois os comandos ou programas podem variar de SO para SO

Este diagrama representa a arquitetura geral do sistema:        

![](/diagramas/full.png)

        

##  1. Programas necessários:
Você irá precisar que estes programas estejam instalados em sua máquina antes de continuarmos.

- Java SE Development Kit 11  
> Link para download(Linux ou Windows): https://www.oracle.com/java/technologies/javase-jdk11-downloads.html

- Docker       
> Link para download Windows: https://docs.docker.com/docker-for-windows/install/       
 Link para download Linux: https://docs.docker.com/engine/install/ubuntu/  
     
- Node
> Link para download(Linux ou Windows): https://nodejs.org/en/download/         



##### 1.1 Verifique se os programas estão rodando corretamente executando os comandos abaixo na console:

> Windows       
>
>       java --version          
>       java version "11.0.9" 2020-10-20 LTS            
>       Java(TM) SE Runtime Environment 18.9 (build 11.0.9+7-LTS)           
>       Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.9+7-LTS, mixed mode)                
>
>       docker -v      
>       Docker version 19.03.13, build 4484c46d9d
>
>       node -v
>       v14.15.0
 
> Linux       
>
>       java --version          
>       java version "11.0.9" 2020-10-20 LTS            
>       Java(TM) SE Runtime Environment 18.9 (build 11.0.9+7-LTS)           
>       Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.9+7-LTS, mixed mode)                
>
>       docker -v      
>       Docker version 19.03.13, build 4484c46d9d
>
>       node -v
>       v14.15.0
    
            

##  2. Configurações iniciais:

#####  2.1 Crie um diretório chamado `\tools`
#####  2.2 Dentro do diretório `\tools` extrair os arquivos: `kafka.zip`,`zookeeper.zip` e `compiled.zip`
```
\tools
    \zookeeper
    \kafka
    \compiled
```
        
##  3. Subindo as aplicações:

#####   Os arquivos compactados `zookeeper.zip` e `kafka.zip` já estão configurados e já contém todos tópicos criados e não é necesário nenhuma configuração adicional.
Mas caso precise configurar do zero, vou estar deixando os links para instalação e configuração abaixo:

> Link para configuração do ZOOKEEPER do Windows:       
> https://medium.com/@shaaslam/installing-apache-zookeeper-on-windows-45eda303e835      
>
> Link para configuração do ZOOKEEPER no Linux:     
>https://www.tutorialspoint.com/zookeeper/zookeeper_installation.htm     
>

> Link para configuração do KAFKA no Windows:     
> https://medium.com/@mmarcosab/usando-apache-kafka-e-apache-zookeeper-no-windows-3e48e76e795f                
>
>Link para configuração do KAFKA no Linux:         
> https://medium.com/@bs745645/como-instalar-o-apache-kafka-no-ubuntu-18-04-b60171500b6e            
              
#####  3.1 Primeiro vamos iniciar o zookeeper (Windows ou Linux).
> Windows
>
> Entre no diretorio `\tools\zookeeper\bin`, abra uma console e execute:
>```
> zkServer.cmd  
>```

> Linux
>
> Entre no diretorio `/tools/kafka/bin`, abra uma console e execute:
>```
> zookeeper-server-start.sh ../config/zookeeper.properties
>```
        
#####  3.2 Depois vamos iniciar o kafka (Windows ou Linux).
> Windows       
> Entre no diretorio `\tools\kafka\bin\windows`, abra uma console e execute:      
> ```
> kafka-server-start.bat ../../config/server.properties
> ```

> Linux       
> Entre no diretorio `/tools/kafka/bin`, abra uma console e execute o seguinte comando para dar permissão de execução ao Kafka:  
> ```
> sudo chmod +x *.sh
> ``` 
> Após execute:  
> ```
> kafka-server-start.sh ../config/server.properties
> ```
        
#####  3.3 Execute o comando abaixo para listar os tópicos recém criados (Windows ou Linux).

> Windows       
> Entre no diretorio `\tools\kafka\bin\windows` e execute:      
> ```
> kafka-topics.bat -bootstrap-server localhost:9092 --list
> ```  

> Linux       
> Entre no diretorio `/tools/kafka/bin` e execute:      
> ```
> kafka-topics.sh -bootstrap-server localhost:9092 --list
> ```
            
                      
                      
Após executar o comando para listar os tópicos, a console deve mostrar algo como:

>       kafka-topics.bat -bootstrap-server localhost:9092 --list        
>       email       
>       executa-pagamento       
>       executa-reserva-ticket-rollback     
>       executa-reserva-tickets     
>       reply-channel       


 
#####  3.4 Caso os tópicos não sejam listados como no exemplo acima, execute os comandos abaixo (Windows ou Linux).

> Windows       
> Entre no diretorio `\tools\kafka\bin\windows`, abra uma console e execute:      
> 
>       kafka-topics.bat -bootstrap-server localhost:9092 --topic reply-channel --create --partitions 1 --replication-factor 1        
>       kafka-topics.bat -bootstrap-server localhost:9092 --topic executa-pagamento --create --partitions 1 --replication-factor 1        
>       kafka-topics.bat -bootstrap-server localhost:9092 --topic executa-reserva-tickets --create --partitions 1 --replication-factor 1      
>       kafka-topics.bat -bootstrap-server localhost:9092 --topic executa-reserva-ticket-rollback --create --partitions 1 --replication-factor 1      
>       kafka-topics.bat -bootstrap-server localhost:9092 --topic email --create --partitions 1 --replication-factor 1        
>       

> Linux       
> Entre no diretorio `/tools/kafka/bin`, abra uma console e execute:      
>       
>       kafka-topics.sh -bootstrap-server localhost:9092 --topic reply-channel --create --partitions 1 --replication-factor 1        
>       kafka-topics.sh -bootstrap-server localhost:9092 --topic executa-pagamento --create --partitions 1 --replication-factor 1        
>       kafka-topics.sh -bootstrap-server localhost:9092 --topic executa-reserva-tickets --create --partitions 1 --replication-factor 1      
>       kafka-topics.sh -bootstrap-server localhost:9092 --topic executa-reserva-ticket-rollback --create --partitions 1 --replication-factor 1      
>       kafka-topics.sh -bootstrap-server localhost:9092 --topic email --create --partitions 1 --replication-factor 1        
>
              

#####  3.5 Vamos subir agora os banco de dados no docker.       
> Abra o console e execute os comandos abaixo (uma console p/ cada comando):      
> 
>       docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name ticketsdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=ticketsdb -p 5437:5432 postgres:10.5      
>       docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name paymentsdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=paymentsdb -p 5433:5432 postgres:10.5        
>       docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name orderdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=orderdb -p 5435:5432 postgres:10.5
>       docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name userdb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=userdb -p 5401:5432 postgres:10.5
>       docker run --ulimit memlock=-1:-1 -it -d --rm=true --memory-swappiness=0 --name imagedb -e POSTGRES_USER=eventos-da-hora -e POSTGRES_PASSWORD=eventos-da-hora -e POSTGRES_DB=imagedb -p 5402:5432 postgres:10.5
>       
        

#####  3.6 Agora vamos subir as aplicações em Java.       
 Entre no diretorio `\tools\compiled` e execute os comandos:        
             
- Acesse o diretório `\zuul-gateway` e execute :    
>       java -jar eventos-da-hora-zuul-gateway.jar      

- Acesse o diretório `\orquestrador-api` e execute :    
>       java -jar eventos-da-hora-orquestrador-sagas.jar

- Acesse o diretório `\image-api` e execute :    
>       java -jar eventos-da-hora-image-api.jar         

- Acesse o diretório `\event-api` e execute :    
>       java -jar eventos-da-hora-event-api.jar         

- Acesse o diretório `\email-api` e execute :    
>       java -jar eventos-da-hora-email-api.jar        

- Acesse o diretório `\payment-api` e execute :    
>       java -jar eventos-da-hora-payment-api.jar   
   
- Acesse o diretório `\order-api` e execute :    
>       java -jar eventos-da-hora-pedido-api.jar            
            
- Acesse o diretório `\user-api` e execute :          
>       java -jar eventos-da-hora-user-api.jar          

        

#####  3.6 Por fim vamos configurar e subir as aplicações de interface.       

##### Executando a interface gráfica do usuario
- Acesse o diretório `\tools\compiled\eventos-da-hora-ui\` e execute :          
>       npm install          
- E depois 
>       node server.js        
O sistema irá subir na porta `localhost:4200`       
        
##### Executando a interface gráfica do administrador:     
- Acesse o diretório `\tools\compiled\eventos-da-hora-admin-ui\` e execute :          
>       npm install          
- E depois 
>       node server.js        
O sistema irá subir na porta `localhost:4300`


##  4. Organização das aplicações:

>- eventos-da-hora-ui   
> Porta da aplicação: 4200      
> Acesso local: http://localhost:4200   

>- eventos-da-hora-admin-ui     
 > Porta da aplicação: 4300     
 > acesso local: http://localhost:4300  

>- eventos-da-hora-zuul-gateway     
> Porta da aplicação: 8000      
> Acesso local: http://localhost:8000

>- eventos-da-hora-user-api     
> Porta da aplicação: 8001   
> Porta do banco de dados: 5401     
> Acesso direto: http://localhost:8001/swagger-ui.html   
> Acesso gateway: http://localhost:8000/eventos-da-hora-user-api/swagger-ui.html    

>- eventos-da-hora-email-api     
> Porta da aplicação: 8002      
> Acesso direto: http://localhost:8002/swagger-ui.html   
> Acesso gateway: http://localhost:8000/eventos-da-hora-email-api/swagger-ui.html    

>- eventos-da-hora-image-api     
> Porta da aplicação: 8003      
> Porta do banco de dados: 5402     
> Acesso direto: http://localhost:8003/swagger-ui.html           
> Acesso gateway: http://localhost:8000/eventos-da-hora-image-api/swagger-ui.html           

>- eventos-da-hora-event-api     
> Porta da aplicação: 8181      
> Porta do banco de dados: 5437     
> Acesso direto: http://localhost:8181/swagger-ui.html        
> Acesso gateway: http://localhost:8000/eventos-da-hora-event-api/swagger-ui.html    

>- eventos-da-hora-orquestrador-sagas     
> Porta da aplicação: 9092      
> Acesso direto: http://localhost:9092/swagger-ui.html   

>- eventos-da-hora-payment-api     
> Porta da aplicação: 8282      
> Porta do banco de dados: 5433     
> Acesso direto: http://localhost:8282/swagger-ui.html          
> Acesso gateway: http://localhost:8000/eventos-da-hora-payment-api/swagger-ui.html         

>- eventos-da-hora-order-api        
> Porta da aplicação: 8187      
> Porta do banco de dados: 5435     
> Acesso direto: http://localhost:8187/swagger-ui.html          
> Acesso gateway: http://localhost:8000/eventos-da-hora-order-api/swagger-ui.html         
---


